FROM debian:12 as base
ARG PROXY=http://oma-proxy.nodeto.site:3128
ENV http_proxy=${PROXY}

RUN apt update \
    && apt-get install -y \
        python3.11 \
        python3.11-venv \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

RUN python3.11 -m venv /app/pipx_venv && \
    . /app/pipx_venv/bin/activate && \
    pip install pipx && \
    pipx install poetry==1.8.2

COPY pyproject.toml poetry.lock README.md ./

RUN python3.11 -m venv /app/venv && \
    . /app/venv/bin/activate && \
    /root/.local/bin/poetry install --only main --no-root --compile

FROM base as app

COPY central_monitor/. ./central_monitor/

RUN . /app/venv/bin/activate && \
    /root/.local/bin/poetry build --format=wheel && \
    pip install --no-deps dist/*

FROM kjoyce77/restic as final
ARG PROXY=http://oma-proxy.nodeto.site:3128
ENV http_proxy=${PROXY}

RUN apt-get update \
    && apt-get install -y \
        python3.11 \
    && rm -rf /var/lib/apt/lists/*

COPY --from=base /app/venv/ /app/venv/
COPY --from=app /app/venv/lib/python3.11/site-packages/central_monitor/ /app/venv/lib/python3.11/site-packages/central_monitor/
COPY log_config.yaml /app/

ENTRYPOINT ["/app/venv/bin/uvicorn", "central_monitor.main:app", "--host", "0.0.0.0", "--log-config", "/app/log_config.yaml"]
