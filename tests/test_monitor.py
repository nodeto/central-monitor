import time
from http import HTTPStatus
from unittest import mock

from fastapi.testclient import TestClient
from httpx import Response

import central_monitor.main
from central_monitor.main import alerts, app, hosts_data
from central_monitor.models import DiskUsage, GlancesData
from central_monitor.utils import extract_glances_data

client = TestClient(app)


def test_update_endpoint():
    # Test the /update/{hostname} endpoint
    hosts_data.clear()
    hostname = "test-host"
    glances_data = {
        # Sample Glances data for testing
        "fs": {"/.percent": 85, "/boot.percent": 70},
        "cpu": {"total": 90, "nice": 5},
        "mem": {"percent": 95},
    }

    response = client.post(f"/update/{hostname}", json=glances_data)
    assert (
        response.status_code == HTTPStatus.NO_CONTENT
    ), f"Bad response, {response.text}"


def test_extract_glances_data():
    # Test the extract_glances_data function
    hosts_data.clear()
    glances_data = {
        "fs": {"/.percent": 85, "/boot.percent": 70},
        "cpu": {"total": 90, "nice": 5},
        "mem": {"percent": 85},
    }

    expected_data = GlancesData(
        disk_usage=[
            DiskUsage(mount_path="/", percent=85),
            DiskUsage(mount_path="/boot", percent=70),
        ],
        cpu_usage=85,
        memory_usage=85,
    )

    assert extract_glances_data(glances_data) == expected_data


def test_status_endpoint_healthy():
    # Test the /status endpoint when all conditions are healthy
    hosts_data.clear()
    response = client.get("/status")
    assert response.status_code == 200
    assert response.text == "OK"


def test_status_endpoint_thresholds():
    """Assert that thresholds are reported."""
    hosts_data.clear()
    glances_data = {
        "fs": {"/.percent": 85, "/boot.percent": 70},
        "cpu": {"total": 90, "nice": 5},
        "mem": {"percent": 95},
    }

    hostname = "test-host"
    client.post(url=f"/update/{hostname}", json=glances_data)

    response = client.get(url="/status")
    assert response.status_code == HTTPStatus.OK
    assert "exceeding threshold for disk: /" in response.text
    assert "exceeding threshold for memory" in response.text
    assert "exceeding threshold for disk: /boot" not in response.text
    assert "exceeding threshold for cpu" not in response.text
    assert hostname in response.text

def test_exclude_endpoint():
    hosts_data.clear()
    glances_data = {
        "fs": {"/opt/storj.percent": 100, "/boot.percent": 60},
        "cpu": {"total": 40, "nice": 5},
        "mem": {"percent": 40},
    }

    hostname = "test-host"
    client.post(url=f"/update/{hostname}", json=glances_data)

    response = client.get(url="/status")
    assert response.status_code == HTTPStatus.OK
    assert response.text == "OK"

def test_cpu_suppression():
    hosts_data.clear()
    monotonic_mock = mock.Mock()
    glances_data = {
        "cpu": {
            "total": 100,
            "nice": 5,
        }
    }

    glances_data_low_cpu = {
        "cpu": {
            "total": 50,
            "nice": 5,
        }
    }

    hostname = "test-host"
    for _ in range(5):
        client.post(url=f"/update/{hostname}", json=glances_data)
    response = client.get(url="/status")
    assert "exceeding threshold for cpu" in response.text

    # now test that a low_cpu within the 1200 second threshold will not trigger a warning
    client.post(url=f"/update/{hostname}", json=glances_data_low_cpu)
    client.post(url=f"/update/{hostname}", json=glances_data)
    response: Response = client.get(url="/status")
    assert "OK" in response.text

    # mock a future time and the single submission should trigger a warning
    monotonic_mock.return_value = time.monotonic() + 1500
    with mock.patch("time.monotonic", monotonic_mock):
        client.post(url=f"/update/{hostname}", json=glances_data)
        response = client.get(url="/status")
        assert "exceeding threshold for cpu" in response.text


def test_post_alert() -> None:
    """Test the alert posts and timeouts happen."""
    alerts.clear()
    response = client.post("/alert", content="test alert")  # type: ignore
    assert response.status_code == HTTPStatus.NO_CONTENT
    assert alerts[0][1] == "test alert"
    assert len(alerts) == 1

    response: Response = client.get(url="/status")
    assert (
        "test alert" in response.text
    ), "Expected 'test alert' to be in status, but it was not."

    monotonic_mock = mock.Mock()
    monotonic_mock.return_value = time.monotonic() + 150
    with mock.patch.object(
        central_monitor.main, "get_monotonic_timestamp", monotonic_mock
    ):
        response: Response = client.get(url="/status")
        assert (
            "test alert" not in response.text
        ), "Expected the test alert to time out, but it didn't."
