import time
from dataclasses import dataclass
from typing import NewType


@dataclass
class DiskUsage:
    mount_path: str
    percent: float


@dataclass
class GlancesData:
    disk_usage: list[DiskUsage]
    cpu_usage: float
    memory_usage: float


MonotonicTimestamp = NewType("MonotonicTimestamp", float)
AlertMessage = NewType("AlertMessage", str)

def get_monotonic_timestamp() -> MonotonicTimestamp:
    return MonotonicTimestamp(time.monotonic())
