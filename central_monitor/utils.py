from typing import Dict
from .models import GlancesData, DiskUsage

def extract_glances_data(data: Dict) -> GlancesData:
    disk_usage = [
        DiskUsage(mount_path=k[:-8], percent=v)
        for k, v in data.get("fs", {}).items()
        if k.endswith(".percent")
    ]
    cpu_usage = data.get("cpu", {}).get("total", 0) - data.get("cpu", {}).get("nice", 0)
    memory_usage = data.get("mem", {}).get("percent", 0)

    return GlancesData(disk_usage=disk_usage, cpu_usage=cpu_usage, memory_usage=memory_usage)
