"""Centralzie glances data."""

import pathlib
import time

import httpx
import tomllib
from fastapi import FastAPI, HTTPException, Request, logger
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.responses import HTMLResponse, PlainTextResponse, Response

from central_monitor.models import (
    AlertMessage,
    GlancesData,
    MonotonicTimestamp,
    get_monotonic_timestamp,
)
from central_monitor.utils import extract_glances_data

app = FastAPI(default_response_class=PlainTextResponse)
app.add_middleware(middleware_class=GZipMiddleware, minimum_size=1000)

# Load the configuration from the TOML file
config = tomllib.loads(
    pathlib.Path(__file__).parent.joinpath("config.toml").read_text()
)


# In-memory storage for host data
hosts_data: dict[str, dict[float, GlancesData]] = {}

# Alerts
alerts: list[tuple[MonotonicTimestamp, AlertMessage]] = []


@app.get("/")
async def root() -> Response:
    """Main page for viewing all servers with glances."""
    html_content = pathlib.Path(__file__).parent / "index.html"
    # copy so the pull down and cookie will be the same
    hosts_data_copy = hosts_data.copy()
    server_content = "\n".join(
        f'<option value="{x}">{x}</option>' for x in hosts_data_copy
    )

    response = HTMLResponse(
        html_content.read_text(encoding="utf-8").replace(
            "{{ servers_html }}", server_content
        )
    )
    if len(hosts_data) == 0:
        return Response(status_code=425)
    server_name = next(iter(hosts_data_copy.keys()))
    response.set_cookie("selected_server", server_name)
    return response


@app.post("/update/{hostname}")
async def update_data(hostname: str, data: dict) -> PlainTextResponse:
    try:
        glances_data = extract_glances_data(data)
        timestamp = time.monotonic()
        if hostname not in hosts_data:
            hosts_data[hostname] = {}
        hosts_data[hostname][timestamp] = glances_data

        # Remove old data points outside the suppression interval
        longest_suppression_interval = max(
            config[x]["suppression"]
            for x in config.keys()
            if "suppression" in config[x]
        )
        hosts_data[hostname] = {
            t: dp
            for t, dp in hosts_data[hostname].items()
            if timestamp - t <= longest_suppression_interval
        }
        logger.logger.debug("Updated data for %s: %s", hostname, glances_data)
        return PlainTextResponse(status_code=204)
    except Exception as exc:
        raise HTTPException(
            status_code=400, detail=f"{exc.__class__.__name__}: {exc}"
        ) from exc


@app.post("/alert")
async def post_alert(request: Request) -> PlainTextResponse:
    """Post an alert message."""
    data = await request.body()
    alert_message = AlertMessage(data.decode("utf-8"))
    alerts.append((get_monotonic_timestamp(), alert_message))
    return PlainTextResponse(status_code=204)


@app.get("/status")
async def get_status() -> str:
    """Return OK if all is well, otherwise a list of alerts."""
    current_time = time.monotonic()
    status_messages = []

    # gather status from glances posts
    for hostname, data_points in hosts_data.items():
        relevant_data_points_by_resource: dict[str, list[float]] = {}
        sorted_data_points = sorted(data_points.items(), reverse=True)
        first_loop = True
        logger.logger.debug("Checking %s: %s", hostname, sorted_data_points)
        for timestamp, glances_data in sorted_data_points:
            time_since_update = current_time - timestamp
            for disk in glances_data.disk_usage:
                if time_since_update < config["disk"]["suppression"] or first_loop:
                    if disk.mount_path not in config["disk"].get("exclude_dirs"):
                        logger.logger.debug(
                            "Adding data point for %s: %s", hostname, disk
                        )
                        relevant_data_points_by_resource.setdefault(
                            f"disk: {disk.mount_path}", []
                        ).append(disk.percent)
                else:
                    logger.logger.debug(
                        "Skipping data point for %s: %s", hostname, disk
                    )

            if time_since_update < config["cpu"]["suppression"] or first_loop:
                relevant_data_points_by_resource.setdefault("cpu", []).append(
                    glances_data.cpu_usage
                )

            if time_since_update < config["memory"]["suppression"] or first_loop:
                relevant_data_points_by_resource.setdefault("memory", []).append(
                    glances_data.memory_usage
                )
            first_loop = False

        logger.logger.debug(
            "Relevant data points for %s: %s",
            hostname,
            relevant_data_points_by_resource,
        )
        for resource, data_points in relevant_data_points_by_resource.items():
            if all(x > config[resource.split(":")[0]]["percent"] for x in data_points):
                status_messages.append(f"{hostname} exceeding threshold for {resource}")

    # gather cron reports
    current_alerts = [
        x
        for x in alerts
        if get_monotonic_timestamp() - x[0] < config["alert_timeout"]["seconds"]
    ]
    status_messages.extend([x[1] for x in current_alerts])

    alerts.clear()
    alerts.extend(current_alerts)

    if not status_messages:
        return "OK"
    return "\n".join(status_messages)


@app.post("/select-server/{server_name}")
def select_server(server_name: str) -> Response:
    """Update the server URL based on the selected server name."""
    # Mapping of server names to URLs
    response = Response(status_code=204)
    response.set_cookie("selected_server", server_name)
    return response


@app.get("/{path:path}")
async def proxy_request(request: Request, path: str) -> Response:
    """A simple proxy."""
    async with httpx.AsyncClient() as client:
        if len(hosts_data) == 0:
            return Response(content="Server not ready, please retry.", status_code=425)
        server_name = request.cookies.get(
            "selected_server", next(iter(hosts_data.keys()))
        )
        server_url = f"http://{server_name}.{config['servers_domain']['domain']}:61208"
        response = await client.get(f"{server_url}/{path}")
        # Consider adding error handling here
        headers = {"content-type": response.headers["content-type"]}
        return Response(
            response.text, status_code=response.status_code, headers=headers
        )
